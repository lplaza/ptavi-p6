#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple client for a SIP registrar
"""

import socket
import sys
from bitstring import BitArray

usage = "Usage: python3 client.py <metido> <receptor>@<IPreceptor>:<puertosIP>"


def conseguir_argv():
    
    if len(sys.argv) != 3:
        sys.exit(usage)
    
    sys.argv.pop(0)
    
    space = Receive.split("@")
    Method = sys.argv.pop(0) # Sirven Bye, Ack e Invite
    Receive = sys.argv.pop(0)
    Address = space[0]
    IP = space[1].split(":")[0]
    Port = int(space[1].split(":")[1])
    return Method, Address, IP, Port

def createRequest (Method, Address, IP, Port):
    
    if Method == "invite":
        message = f"{Method.upper()} sip:{Address}@{IP} SIP/2.0\n"
        body = f"v=0\no=robin@gohtam.com {IP}\ns=misesion\nt=0\nm=audio {Port} RTP\n"
        size = len(bytes(cuerpo, "utf-8"))
        head = f"Content-type: application/sdp\nContent-Lenght: {size}\n\n"
        Rq= message + head + body
    else:
        Rq = f"{Method.upper()} sip:{Address}@{IP} SIP/2.0\n\n"
    
    return request

def main():
    Method, Address, IP, Port = conseguir_argv()
    Rq = createRequest(Method, Address, IP)
    
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as main_socket:
        main_socket.sendto(Rq.encode("utf-8"), (IP, Port))
        respuesta = main_socket.recv(2048)
        print(respuesta.decode("utf-8"))
        metodo = respuesta.decode("utf-8").split()[1]
        if metodo == "200" and metodo == "invite":
            mensaje = f"ACK sip:{Address}@{IP} SIP /2.0"
            main_socket.sendto(mensaje.encode("utf-8"), (IP, Port))
            
if __name__ == "__main__":
    main()
