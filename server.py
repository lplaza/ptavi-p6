#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple SIP registrar
"""
import socketserver
import sys
import simplertp
import random

class SIPHandler(socketserver.BaseRequestHandler):
    
    def handle(self):
        valid_method = ("INVITE", "BYE", "ACK")
        data = self.request[0]
        sock = self.request[1]
        returnal = data.decode("utf-8")
        
        print(f"{self.client_address[0]} {self.client_address[1]} {returnal}")
        
        Method = returnal.split()[0]
        
        if type(returnal) is not str:
            respuesta = "SIP/2.0 400 Bad Request"
            sock.sendto(respuesta.encode("utf-8"), self.client_address)
        
        if Method not in valid_method:
            respuesta = "SIP/2.0 405 Method Not Allowed"
            sock.sendto(respuesta.encode("utf-8"), self.client_address)
        
        if Method == valid_method[0]:
            trying = "SIP/2.0 100 TRY"
            sock.sendto(trying.encode("utf-8"), self.client_address)
            body = "\n".join(returnal.split("\n")[1:])
            respuesta = "SIP/2.0 200 OK\n" + body
            sock.sendto(respuesta.encode("utf-8"), self.client_address)
        
        if Method == valid_method[1]:
            sock.sendto(f"SIP/2.0 OK".encode(), self.client_address)
            
        if Method == valid_method[2]:
            IP = self.client_address[0]
            Port = self.client_address[1]
            archivo = sys.argv[3]
            ALEAT = random.randint(0, 10000)
            
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag = 0, ext_flag = 0, cc = 0, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(file)
            simplertp.send_rtp_packet(RTP_header, audio, IP, Port)
            
def main():
    
    texto = "Usage: python3 server.py <IP> <puerto> <fichero_audio>"
    
    if len(sys.argv) !=4:
        sys.exit(texto)
        
    try:
        IP, Port, file = sys.argv[1:]
        Port = int(Port)
    
    except ValueError:
        sys.exit("El puerto tiene un valor incompatible")
        
    try:
        server = socketserver.UDPServer((IP, Port), SIPHandler)
        print(f"Listening port {Port}")
    
    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}")
    
    try:
        serv.serve_forever()
    
    except KeyboardInterrupt:
        print("Server done")
        sys.exit(0)
        
if __name__ == "__main__":
    main()
